clc
clear

% DefaultName = pwd;
DefaultName = '/media/penguin/Data/Work/Swiss/2015_12_08';
[filename,path]=uigetfile('*.png','Get the input image file',DefaultName);
initial_Image_name = strcat(path,filename);

% initial_Image = imread('../1000/Processed/2015_10_29_FRcontrol_oscillations_10000000.png');
% initial_Image = imread('../1000/Enlarged_and_Blurred/Image_0000.png');
initial_Image = imread(initial_Image_name);
h1 = figure;
imshow(initial_Image)

%%%%%%%%%%%%%%%%%%%%%%%%%%
num_points = 2;
%%%%%%%%%%%%%%%%%%%%%%%%%%

% get x,y coordinates and then define template by adding 10 pixels on each
% side.

[x,y] = ginput(num_points);
X = floor(x); Y = floor(y);
close(h1)

[Output_dirname, output_fname] = initiate_file_writing(num_points);

hFig = figure('Visible','off');
hAx  = axes;
imshow(initial_Image,'Parent', hAx);

Templates = zeros(num_points*21, 31);
for i = 1:num_points
    template = initial_Image(Y(i)-10 : Y(i)+10, X(i)-15 : X(i)+15);
    Templates(21*(i-1)+1 : 21*i, 1:31) = template;
    
    yoffSet = Y(i)-size(template,1)/2;
    xoffSet = X(i)-size(template,2)/2;
%     imrect(hAx, [xoffSet, yoffSet, size(template,2), size(template,1)]);
    rectangle('Position', [xoffSet, yoffSet, size(template,2), size(template,1)], 'EdgeColor', 'r');

end

output_image_name = strcat(Output_dirname, '/image_templates.png');
saveas(hFig, output_image_name)
close(hFig)

d = dir(path);
num_images = length(d) - 6;

% fid  = fopen(output_fname, 'a');
% num_images = 1000;

% for counter = 0:5
for counter = 0:num_images

    counter_str = sprintf('%04d', counter);
    im_name = strcat(path, filename(1: length(filename)-8), counter_str, '.png');
    target_Image = imread(im_name);

    % Performing the cross-correlation
    
    Y_peak = zeros(num_points, 1);
    X_peak = zeros(num_points, 1);
    
    array_to_write = zeros(1, 2*num_points);
    
    for i = 1:num_points
        
        template = Templates(21*(i-1)+1 : 21*i, 1:31);
        
        return_array = extract_region_to_match(X(i), Y(i), target_Image);
        small_image = target_Image(return_array(1) : return_array(2), return_array(3): return_array(4));
        
        c = normxcorr2(template, small_image);
        [y_from_c, x_from_c] = find(c==max(c(:)));
        
        X_peak(i) = return_array(3) + x_from_c;
        Y_peak(i) = return_array(1) + y_from_c;
        
        array_to_write(2*i-1) = X_peak(i);
        array_to_write(2*i) = Y_peak(i);
        
    end
    array_to_write(2*num_points+1) = Y_peak(num_points) - Y_peak(num_points-1);
    dlmwrite(output_fname, array_to_write, '-append');
    
%     yoffSet = ypeak-size(template,1);
%     xoffSet = xpeak-size(template,2);
% 
%     hFig = figure('Visible','off');
%     hAx  = axes;
%     imshow(target_Image,'Parent', hAx);
%     imrect(hAx, [xoffSet, yoffSet, size(template,2), size(template,1)]);
%     
%     output_fig_name = strcat('output/output_image_', counter_str, '.png');
%     saveas(hFig, output_fig_name)
%     close(hFig)
    
%     fprintf(fid, '%f,', xpeak1);
%     fprintf(fid, '%f,', ypeak1);
%     fprintf(fid, '%f,', xpeak2);
%     fprintf(fid, '%f\n', ypeak2);

end
% fclose(fid);
disp(['images read from ', path])
disp(['output stored in the directory ', Output_dirname])
