function return_array = extract_region_to_match(x_loc, y_loc, big_image)

% Return an image of size 151x101 around the location x_loc and y_loc.

[size_y, size_x] = size(big_image);

x_range_width = 20;
y_range_width = 55;

left_limit = x_loc - x_range_width;
right_limit = x_loc + x_range_width;
up_limit = y_loc - y_range_width;
down_limit = y_loc + y_range_width;


% check left
if x_loc - x_range_width <= 0
    left_limit = 1;
end

% check right
if x_loc + x_range_width >= size_x
    right_limit = size_x;
end

% check up
if y_loc - y_range_width <= 0
    up_limit = 1;
end

% check down
if y_loc + y_range_width >= size_y
    down_limit = size_y;
end

return_array = [up_limit; down_limit; left_limit; right_limit];

end