function return_array = extract_region_to_match(x_loc, y_loc, big_image)

% Returns the four vertices of image around x_loc, y_loc that should be
% used for matching

[size_y, size_x] = size(big_image);

x_range_width = 20;
y_range_width = 50;

left_limit = x_loc - x_range_width;
right_limit = x_loc + x_range_width;
up_limit = y_loc - y_range_width/2;
down_limit = y_loc + y_range_width;


% check left
if left_limit <= 0
    left_limit = 1;
end

% check right
if right_limit >= size_x
    right_limit = size_x;
end

% check up
if up_limit <= 0
    up_limit = 1;
end

% check down
if down_limit >= size_y
    down_limit = size_y;
end

return_array = [up_limit; down_limit; left_limit; right_limit];

end