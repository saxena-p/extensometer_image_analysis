clc
clear

% DefaultName = pwd;
% DefaultName = '/media/prashant/DATAPART1/Data/2015_10_29_Bern/tiffs';
% [filename,path]=uigetfile('*.png','Get the initial image file',DefaultName);
% initial_Image_name = strcat(path,filename);
% 
% [filename2,path2]=uigetfile('*.png','Get the final image file',DefaultName);
% target_Image_name = strcat(path2,filename2);

initial_Image_name = '/media/prashant/DATAPART1/Data/2015_10_29_Bern/tiffs/Piclo3_oscillations_together/Image_2712.png';
target_Image_name = '/media/prashant/DATAPART1/Data/2015_10_29_Bern/tiffs/Piclo3_oscillations_together/Image_1959.png';


% initial_Image = imread('../1000/Processed/2015_10_29_FRcontrol_oscillations_10000000.png');
% initial_Image = imread('../1000/Enlarged_and_Blurred/Image_0000.png');
initial_Image = imread(initial_Image_name);
target_Image = imread(target_Image_name);

h1 = figure;
imshow(initial_Image)

%%%%%%%%%%%%%%%%%%%%%%%%%%
num_points = 2;
%%%%%%%%%%%%%%%%%%%%%%%%%%

% get x,y coordinates and then define template by adding 10 pixels on each
% side.

[x,y] = ginput(num_points);
X = floor(x); Y = floor(y);
close(h1)

% make the output spreadsheet with the headers
[Output_dirname, output_fname] = initiate_file_writing(num_points);

% Make a figure of the initial image and save
hFig = figure('Visible','off');
hAx  = axes;
imshow(initial_Image,'Parent', hAx);

Templates = zeros(num_points*21, 31);
for i = 1:num_points
    template = initial_Image(Y(i)-10 : Y(i)+10, X(i)-15 : X(i)+15);
    Templates(21*(i-1)+1 : 21*i, 1:31) = template;
    
    yoffSet = Y(i)-size(template,1)/2;
    xoffSet = X(i)-size(template,2)/2;
%     imrect(hAx, [xoffSet, yoffSet, size(template,2), size(template,1)]);
    rectangle('Position', [xoffSet, yoffSet, size(template,2), size(template,1)], 'EdgeColor', 'r');

end

output_image_name = strcat(Output_dirname, '/image_templates_source.png');
saveas(hFig, output_image_name)
close(hFig)


% Performing the cross-correlation

Y_peak = zeros(num_points, 1);
X_peak = zeros(num_points, 1);

array_to_write = zeros(1, 2*num_points);
initial_points = zeros(1, 2*num_points);

for i = 1:num_points

    template = Templates(21*(i-1)+1 : 21*i, 1:31);

    return_array = extract_region_to_match(X(i), Y(i), target_Image);
    small_image = target_Image(return_array(1) : return_array(2), return_array(3): return_array(4));

    c = normxcorr2(template, small_image);
    [y_from_c, x_from_c] = find(c==max(c(:)));

    X_peak(i) = floor( return_array(3) + x_from_c - size(template, 2)/2);
    Y_peak(i) = floor( return_array(1) + y_from_c - size(template, 1)/2);

    array_to_write(2*i-1) = X_peak(i);
    array_to_write(2*i) = Y_peak(i);
    
    initial_points(2*i-1) = X(i);
    initial_points(2*i) = Y(i);

end
dlmwrite(output_fname, initial_points, '-append');
dlmwrite(output_fname, array_to_write, '-append');


% Make a figure of the output image and save

hFig = figure('Visible','off');
hAx  = axes;
imshow(target_Image,'Parent', hAx);

for i = 1:num_points
    x_val = array_to_write(2*i-1);
    y_val = array_to_write(2*i);
    template = target_Image(y_val-10 : y_val+10, x_val-15 : x_val+15);

    yoffSet = y_val-size(template,1)/2;
    xoffSet = x_val-size(template,2)/2;
%     imrect(hAx, [xoffSet, yoffSet, size(template,2), size(template,1)]);
    rectangle('Position', [xoffSet, yoffSet, size(template,2), size(template,1)], 'EdgeColor', 'r');

end

output_image_name = strcat(Output_dirname, '/image_templates_target.png');
saveas(hFig, output_image_name)
close(hFig)

% disp(['images read from ', path])
disp(['output stored in the directory ', Output_dirname])
% clear
