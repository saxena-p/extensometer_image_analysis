function [Output_dirname, output_fname] = initiate_file_writing(num_points)

% first make the output folder
counter = 0;
is_output = 1;
while is_output ~= 0
    
    counter = counter + 1;
    Output_dirname = strcat('output_', num2str(counter));
    is_output = exist(Output_dirname, 'dir'); 
    
end
mkdir(Output_dirname);

% define the file in which to write the output data
output_fname = strcat(Output_dirname, '/summary.csv');

% Make the title array
titles = {};
for i = 1:num_points
    
    xlabel = strcat('X_Point_', num2str(i));
    ylabel = strcat('Y_Point_', num2str(i));
    
    titles(2*i -1) = {xlabel};
    titles(2*i) = {ylabel};
    
end

% Write all the titles on the first row. These are only for the human eye
fid = fopen(output_fname, 'w');
fprintf(fid, '%s,', titles{1, 1:end-1});
fprintf(fid, '%s\n', titles{1, end});
fclose(fid);


end